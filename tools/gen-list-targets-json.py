import json
import psycopg2

class Object:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)

req = """SELECT
	t_kassa.ip, 
	t_kassa.cash_box, 
	t_kassa.trade_point, 
	t_trade_point."name"
FROM
	t_trade_point
	INNER JOIN
	t_kassa
	ON 
		t_trade_point.trade_point = t_kassa.trade_point"""

req_servers = """SELECT
	t_server.ip, 
	'SERVER' as server, 
	t_trade_point.trade_point, 
	t_trade_point."name"
FROM
	t_server
	INNER JOIN
	t_trade_point
	ON 
		t_server.trade_point = t_trade_point.trade_point
                """

conn = psycopg2.connect(dbname='db_admin', user='postgres', host='192.168.1.14')
cursor = conn.cursor()
cursor.execute(req_servers)
#records = cursor.fetchall()
lis = []
for row in cursor:
    print(row)
    test = Object()
    test.targets = [str(row[0])]
    test.labels = {
            "id_pos": str(row[1]),
            "id_shop": str(row[2]),
            "shop_name": str(row[3])
        }
    lis.append(test.__dict__)
cursor.close()
conn.close()


ob_j = json.dumps(lis, ensure_ascii=False, indent=2)

print(ob_j)

jsonFile = open("data.json", "w")
jsonFile.write(ob_j)
jsonFile.close()
